<img src="./laptop.svg" min-width="340px" max-width="400px" width="340px" align="right" alt="laptop">

<p align="left"> 
I'm <strong>Mehfooz-ur-Rehman</strong> Web Developer and UI/UX Designer from Samundri, Pakistan, working in Haramosh Studio startup based in Samundri, Pakistan. I enjoy turning complex problems into simple, beautiful and intuitive solutions.
 
My job is to build functional and user-friendly and at the same time attractive websites for you. Moreover, I add a personal touch to your product and make sure that it is eye-catching and easy to use. My aim is to bring across your message and identity in the most creative way.
</p>

<p align="left">
  🦄 <strong>Skills:</strong> HTML5, CSS3, JavaScript, Bootstrap, ReactJS, MongoDB, ExpressJS, NodeJS, Typescript, Graphql, Rest.
</p>

<p align="left">
  💼 <strong>Tools:</strong> Visual Studio Code, Android Studio, GitHub Desktop, Windows Terminal, Firebase, Netlify, Adobe XD and Git.
</p>

Emails:

- mehfoozijaz786@gmail.com
- mehfooz_ur_rehman@outlook.com
